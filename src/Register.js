import React, { Component } from 'react';
import { View, Text, TouchableOpacity, StyleSheet, TextInput, Image } from 'react-native';
import { black } from 'ansi-colors';

export default class Register extends Component {
    static navigationOptions = {
        title: null,
        header: null
    };
    constructor(props) {
        super(props);
        this.state = {
        };
    }

    loginCheck = () => {
        alert('Registration not allowed now')
        //this.props.navigation.navigate('Questionaries')
    }

    render() {
        const { navigate } = this.props.navigation;
        return (
            <View style={styles.container}>
                <Image
                    style={{ width: '40%', height: 30, marginBottom: 40, marginTop: '30%' }}
                    source={require('./images/passtheot-logo.png')}
                />
                <Text style={styles.titleCont}>REGISTER</Text>
                <TextInput
                    placeholder={'UserName'}
                    placeholderTextColor={'#fff'}
                    style={styles.formInputs} />
                <TextInput
                    placeholder={'Password'}
                    placeholderTextColor={'#fff'}
                    style={styles.formInputs} />
                <TextInput
                    placeholder={'Confirm Password'}
                    placeholderTextColor={'#fff'}
                    style={styles.formInputs} />
                <TouchableOpacity
                    style={styles.loginButton}
                    onPress={() => this.loginCheck()}>
                    <Text style={styles.buttontext}> Register </Text>
                </TouchableOpacity>
                <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'flex-start', paddingTop: 40, width: '100%', paddingLeft: '10%' }}>
                    <TouchableOpacity
                        onPress={() => navigate('Login')}
                        style={{ justifyContent: 'flex-start' }}>
                        <Text style={{ color: '#fff' }}>
                            Login
            </Text>
                    </TouchableOpacity>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        flexDirection: 'column',
        alignItems: 'center',
        backgroundColor: '#213466'
    },
    loginButton: {
        fontSize: 19,
        fontWeight: 'bold',
        backgroundColor: '#fff',
        paddingHorizontal: 20,
        paddingVertical: 7,
        borderRadius: 80,
        marginTop: 20
    },
    buttontext: {
    },
    formInputs: {
        width: '80%',
        borderBottomWidth: 1,
        borderColor: '#ccc',
        marginBottom: 20,
        color: '#fff'
    },
    titleCont: {
        color: '#fff',
        fontSize: 24,
        marginBottom: 40
    }
});