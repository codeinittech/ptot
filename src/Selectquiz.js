/* eslint-disable react-native/no-inline-styles */
import React, {Component} from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  Alert,
  StyleSheet,
  Image,
} from 'react-native';
import RNPickerSelect from 'react-native-picker-select';
import {baseUrl} from './constants/HttpConsts';

export default class SelectQuiz extends Component {
  static navigationOptions = {
    title: null,
    header: null,
  };
  constructor(props) {
    super(props);
    this.state = {
      categories: [{label: 'Selet Category', value: 'select Category'}],
      quizzes: '',
      wholeData: '',
      quizToStart: '',
      selectedQuiz: [{label: 'Selet a category first', value: 'select Quiz'}],
      quizName: '',
    };
  }

  componentDidMount() {
    this.fetchingCategories();
  }

  fetchingCategories = () => {
    fetch(
      baseUrl +
        '/get_category_quiz/?secret_key=r4zJswPsVA9KKlDMSbA8sPbXAdw7YMfQ',
    )
      .then(response => response.json())
      .then(responseJson => {
        //console.log(responseJson);
        // pass a function to map
        this.setState({wholeData: responseJson.data})
        let catnames = [];
        responseJson.data.map(function(catData){
          //console.log(catData.category_name);
          catnames.push({
            label: catData.category_name,
            value: catData.category_id,
          });
        });
        //console.log(catnames);
        this.setState({categories: catnames});
      })
      .catch(error => {
        console.error(error);
      });
  };

  fetchingQuizzes = catId => {
    //Alert.alert('Quizzes are fetched ' + catId);
    //console.log(this.state.wholeData)

    let quizfinalised = [];

    this.state.wholeData.map(function(catData) {
      if (
        catData.category_id == catId &&
        typeof catData.quiz.quiz_name !== 'undefined'
      ) {
        quizfinalised.push({
          label: catData.quiz.quiz_name,
          value: catData.quiz.quiz_id,
        });
      }
    });
    this.setState({selectedQuiz: quizfinalised});
  };

  selectedCateory = () => {
    //console.log('Quiz Id from Select Q screen====>',this.state.selectedQuiz[0].label);
    this.setState({
      quizToStart: this.state.selectedQuiz[0].value,
      quizName: this.state.selectedQuiz[0].label,
    });
  };

  stertQuizz = () => {
    if (this.state.quizToStart === '') {
      Alert.alert('Alert', 'Please select a Category and a Quiz');
      return;
    }
    this.props.navigation.navigate('Questionaries', {
      quizId: this.state.quizToStart,
      quizName: this.state.quizName,
    });
  };

  render() {
    return (
      <View style={styles.container}>
        <Image
          style={styles.logoImage}
          source={require('./images/passtheot-logo.png')}
        />
        <View style={styles.formWrapper}>
          <Text style={{color: '#fff'}}> Select a Category </Text>
          <View style={styles.ddWrapper}>
            <RNPickerSelect
              onValueChange={value => this.fetchingQuizzes(value)}
              items={this.state.categories}
            />
          </View>
          <Text style={{color: '#fff', marginTop: 10}}> Select a Quiz </Text>
          <View style={styles.ddWrapper}>
            <RNPickerSelect
              style={{color: 'black'}}
              onValueChange={value => this.selectedCateory()}
              items={this.state.selectedQuiz}
            />
          </View>
        </View>

        <TouchableOpacity
          style={styles.loginButton}
          onPress={() => this.stertQuizz()}>
          <Text style={styles.buttonsText}>START QUIZ</Text>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    flexDirection: 'column',
    alignItems: 'center',
    backgroundColor: '#213466',
  },
  buttonsText: {
    fontSize: 20,
    color: '#fff',
  },
  loginButton: {
    backgroundColor: '#49d04a',
    paddingHorizontal: 40,
    paddingVertical: 8,
    borderRadius: 50,
    marginTop: 20,
  },
  formWrapper: {
    paddingHorizontal: 20,
    width: '100%',
  },
  ddWrapper: {
    backgroundColor: '#fff',
    borderRadius: 10,
    marginVertical: 10,
  },
  logoImage: {
    marginBottom: 100,
  },
});
