import React, {Component} from 'react';
import {ScrollView, Text} from 'react-native';

export default class Terms extends Component {
  static navigationOptions = {
    title: 'Login',
  };
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      // eslint-disable-next-line react-native/no-inline-styles
      <ScrollView style={{flex: 1, padding: 20}}>
        <Text style={{fontSize: 30, paddingBottom: 30, textAlign: 'center'}}>
          Terms and Conditions
        </Text>
        <Text>
          All contents copyrighted by Pass the OT. All rights reserved.
        </Text>
        <Text />
        <Text />
        <Text />
        <Text>PLEASE READ CAREFULLY</Text>
        <Text />
        <Text>
          This Web site is protected by the U.S. and foreign copyright laws.
          Except for informational, personal, non-commercial use as authorized
          below, you may not modify, reproduce, or distribute the design or
          layout of this Web site, or individual sections of the design or
          layout of the Web site or Pass the OT logos, or any content or other
          materials on this Web site including charts, audio recordings, test
          questions, and worksheets, without prior written permission from PASS
          THE OT, unless otherwise indicated below.
        </Text>
        <Text />
        <Text>
          Copyright is not claimed as to any part of an original work prepared
          by a U.S. or state government office or employee as part of that
          person’s official duties. All rights are reserved and content may not
          be reproduced, downloaded, disseminated, or transferred in any form or
          by any means, except with the prior written permission of PASS THE OT
          or as indicated below. Users may download pages or other content for
          their own personal use on a single computer, but no part of such
          content may be otherwise or subsequently reproduced, downloaded,
          disseminated, or transferred, in any form or by any means, except with
          the prior written agreement of, and with express attribution to PASS
          THE OT.
        </Text>
        <Text />
        <Text>DISCLAIMER</Text>
        <Text />
        <Text>
          WHILE PASS THE OT MAKES EVERY EFFORT TO PRESENT ACCURATE AND RELIABLE
          INFORMATION ON THIS SITE, PASS THE OT DOES NOT ENDORSE, APPROVE, OR
          CERTIFY SUCH INFORMATION, NOR DOES IT GUARANTEE ITS ACCURACY,
          COMPLETENESS, EFFICACY, OR TIMELINESS. REFERENCE HEREIN TO ANY
          COMMERCIAL PRODUCT, PROCESS, OR SERVICE DOES NOT CONSTITUTE OR IMPLY
          ENDORSEMENT OR RECOMMENDATION BY PASS THE OT UNLESS EXPRESSLY STATED.
        </Text>
        <Text />
        <Text>
          PASS THE OT ASSUMES NO RESPONSIBILITY FOR CONSEQUENCES RESULTING FROM
          USE OF THE INFORMATION CONTAINED HEREIN OR OBTAINED AT LINKED SITES
          AND EXPRESSLY DISCLAIMS ALL LIABILITY FOR DAMAGES ARISING OUT OF USE,
          REFERENCE TO, RELIANCE ON, OR PERFORMANCE OF SUCH INFORMATION.
        </Text>
        <Text />
        <Text>
          PASS THE OT SPECIFICALLY DISCLAIMS ANY LIABILITY, WHETHER BASED IN
          CONTRACT, TORT, STRICT LIABILITY OR OTHERWISE, FOR ANY DIRECT,
          INDIRECT, INCIDENTAL, CONSEQUENTIAL OR SPECIAL DAMAGES ARISING OUT OF
          IN ANY WAY CONNECTED WITH ACCESS TO OR USE OF THIS WEB SITE OR ANY
          CONTENT, INFORMATION, MATERIALS OR SERVICES HEREIN.
        </Text>
        <Text />
        <Text>USER RESPONSIBILITIES</Text>
        <Text />
        <Text>
          Any person using this system consents to having their activities on
          this system monitored and recorded by systems personnel. If monitoring
          reveals possible evidence of illegal activity, system personnel may
          provide the evidence from such monitoring to PASS THE OT
          administration and/or law enforcement officials. Any person using this
          system agrees to take any reasonable precaution to prevent
          unauthorized access to any passwords, user identification, logon
          identification or other information that may be used to access the
          system. Any person using this system agrees to treat all information
          within the system as proprietary or confidential information to PASS
          THE OT, unless such information is specifically identified by PASS THE
          OT as not proprietary or confidential.
        </Text>
        <Text />
        <Text>LINKS</Text>
        <Text />
        <Text>
          At certain places in this site, links to other Web sites can be
          accessed. These sites contain information created, published, and
          maintained by organizations independent of PASS THE OT. Unless
          specified by PASS THE OT, PASS THE OT does not endorse, approve,
          certify, or control these external sites, nor any commercial product
          or service referenced therein, and does not guarantee the accuracy,
          completeness, efficacy, or timeliness of information located therein.
        </Text>
        <Text />
        <Text>Linking to the PASS THE OT Web site</Text>
        <Text />
        <Text>
          Unless otherwise set forth in a written agreement between user and the
          Association, users must adhere to the Association’s linking policy as
          follows:
        </Text>
        <Text />
        <Text>
          Any links to the Association’s Web site must be a text-only link
          clearly marked “Pass the OT.”
        </Text>
        <Text>
          The appearance, position and other aspects of the link may not be such
          as to damage or dilute the goodwill associated with Pass the OT’s
          names and trademarks.
        </Text>
        <Text>
          The appearance, position, and other attributes of the link may not
          create the false appearance that your organization or entity is
          sponsored by, affiliated with, or associated with Pass the OT
        </Text>
        <Text>
          When selected by a user, the link must display the Web site on full
          screen and not within a “frame” on the linking Web site.
        </Text>
        <Text>
          Links that point to the URL [http://www.Pass the OT.org/] and adhere
          to PASS THE OT’s linking policy do not require written permission.
          Requests for permission to link to any other pages within PASS THE
          OT’s Web site must be made in writing.
        </Text>
        <Text>
          Pass the OT reserves the right to revoke its consent to the link at
          any time and in its sole discretion. PASS THE OTare a few registered
          trademarks or service marks of the Association. Unauthorized use of
          any trademark, service mark or logo may be a violation of federal and
          state trademark laws.
        </Text>
        <Text>
          DISCUSSION FORUMS AND SPECIAL INTEREST SECTION (SIS) LISTSERVERS
        </Text>
        <Text />
        <Text>
          Discussion forums and electronic mailing lists provide an opportunity
          to exchange ideas, share experiences, and offer solutions to everyday
          workplace issues. PASS THE OT prohibits the posting of messages
          intended to serve as product, service, corporate, or
          jobadvertisements. Furthermore, PASS THE OT prohibits the posting of
          any material which infringes the copyrights or other proprietary
          rights of another, or which is obscene, vulgar, immoral, libelous, or
          contains language which is offensive to any racial, ethnic, or
          religious group. While PASS THE OT retains the right to remove any
          messages from this site that do not meet with the purposes stated
          above, PASS THE OT cannot review each message to determine its
          accuracy or truthfulness, or to determine if the content of any
          message contains any prohibited material. Consequently, any reader who
          is offended by any message, or who finds that any message violates the
          guidelines set forth above, should report the message to
          jshane@passtheot.com.
        </Text>
        <Text />
        <Text>E-MAIL</Text>
        <Text />
        <Text>
          The Internet is a public medium and electronic mail communication
          between user and PASS THE OT via this Site is subject to the risk of
          being viewed by other parties. Accordingly, all users acknowledge that
          any transmissions of confidential or proprietary information to PASS
          THE OT via electronic mail are at users’ own risk.
        </Text>
        <Text />
        <Text>COPYRIGHT POLICY</Text>
        <Text />
        <Text>Copyright © Pass the OT LLC 2018 All Rights Reserved</Text>
        <Text />
        <Text />
        <Text>
          All files and information contained in this Website or Blog are
          copyright by Pass the OT LLC, and may not be duplicated, copied,
          modified or adapted, in any way without our written permission. Our
          Website or Blog may contain our service marks or trademarks as well as
          those of our affiliates or other companies, in the form of words,
          graphics, and logos. Your use of our Website, Blog or Services does
          not constitute any right or license for you to use our service marks
          or trademarks, without the prior written permission of Pass the OT
          LLC. Our Content, as found within our Website, Blog and Services, is
          protected under the United States and foreign copyrights. The copying,
          redistribution, use or publication by you of any such Content, is
          strictly prohibited. Your use of our Website and Services does not
          grant you any ownership rights to our Content. Any user who
          distributes or posts any of our study material or content without
          previous written consent will be held liable for damages and
          prosecuted to the full extent of the law.
        </Text>
        <Text />
        <Text />
        <Text />
        <Text>CANCELATION POLICY</Text>
        <Text />
        <Text>
          You may cancel your membership, effective the next billing cycle, by
          clicking “My Account “and hitting cancel membership or by contacting
          our Customer Service Department via email at support@passtheot.com
        </Text>
        <Text />
        <Text>
          If you purchased our CEU program, and no CEU credits have been awarded within 30 days of you signing up. you will be entitled to a full refund.
        </Text>
        <Text />
        <Text />
        <Text />
        <Text>ENTIRE AGREEMENT</Text>
        <Text />
        <Text>
          By viewing, visiting or otherwise using this Site, users agree to all of the terms and conditions set forth in this Disclaimer. Except as expressly provided in a particular “legal notice” on this Site, this Disclaimer constitutes the entire agreement between user and PASS THE OT with respect to the use of this Site and Content.
        </Text>
        <Text />
        <Text>
          Notwithstanding the foregoing, PASS THE OT reserves the right to modify the Site and/or the Disclaimer at any time and users are deemed to be apprised of and bound by any such modifications. If any provision of this Disclaimer shall be held to be invalid, illegal, or unenforceable for any reason, the validity, legality and enforceability of the remaining provisions shall not in any way be affected or impaired thereby.
        </Text>
      </ScrollView>
    );
  }
}
