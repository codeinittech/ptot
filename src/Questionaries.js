/* eslint-disable react-native/no-inline-styles */
import React, {Component} from 'react';
import {
  View,
  Text,
  StyleSheet,
  ScrollView,
  TouchableOpacity,
  Alert,
  Modal,
} from 'react-native';
import {baseUrl} from './constants/HttpConsts';
import {CheckBox} from 'react-native-elements';
import Spinner from 'react-native-loading-spinner-overlay';
import {TextInput} from 'react-native-gesture-handler';
import AsyncStorage from '@react-native-community/async-storage';

export default class Questionaries extends Component {
  static navigationOptions = {
    title: 'All Quizzes',
  };

  componentDidMount() {
    const {quizId} = this.props.navigation.state.params;
    const {quizName} = this.props.navigation.state.params;
    console.log('Selected QuizID ==>', quizId, quizName);
    this.fetchingQuizData(quizId, quizName);
  }

  constructor(props) {
    super(props);
    this.state = {
      option1: false,
      option2: false,
      option3: false,
      option4: false,
      question: '',
      allAnswers: '',
      totalQuestions: '',
      allQuestions: '',
      activeQn: 1,
      activeQnCorrectAns: null,
      currentQnId: '',
      modalVisible: true,
      reportQnModalVisible: false,
      answerData: [],
      quizName: '',
      modalMessage: 'Loading Quiz...',
      spinner: false,
      reportReason: null,
    };
  }

  fetchingQuizData = (loadQuizId, loadQuizName) => {
    this.setState({quizId: loadQuizId, quizName: loadQuizName});
    let dataFetchUrl =
      baseUrl +
      '/get_quiz_questions/?secret_key=r4zJswPsVA9KKlDMSbA8sPbXAdw7YMfQ&loadQuizId=' +
      loadQuizId;
    console.log(dataFetchUrl);
    fetch(dataFetchUrl)
      .then(response => response.json())
      .then(responseJson => {
        console.log(responseJson);
        let finalData = responseJson;
        //console.log( finalData )
        //console.log(finalData.data.details[2]);
        console.log('answeres onload===>', finalData.data.details[0].answer);
        this.setState({
          question: finalData.data.details[0].question,
          totalQuestions: finalData.data.details.length,
          allQuestions: finalData,
          allAnswers: finalData.data.details[0].answer,
          currentQnId: finalData.data.details[0].quizId,
          modalVisible: false,
        });
      })
      .catch(error => {
        console.error(error);
      });
  };

  loadNextQuestion = nextQn => {
    if (this.state.totalQuestions === nextQn - 1) {
      this.submittingAnswers();
    } else {
      this.ShowQuestion(nextQn);
    }
  };

  updatingOptions = optionNoval => {
    let activeQuestionIndex = this.state.activeQn - 1;

    this.setState({
      activeQnCorrectAns: this.state.allQuestions.data.details[
        activeQuestionIndex
      ].correct_answer,
    });
    //Alert.alert('Question ID: ' + this.state.currentQnId,);
    let qstnIndxExists = false;
    let activeQnIndex = this.state.activeQn;
    let savedAnsData = this.state.answerData;
    //console.log(savedAnsData);
    let AnsweredQnData = {
      questionID: this.state.currentQnId,
      questionIndex: activeQnIndex,
      optionNo: [optionNoval],
    };

    console.log('saved ans data ===>',AnsweredQnData);

    if (savedAnsData.length > 0) {
      console.log(savedAnsData.length);

      savedAnsData.map(function(values, svdAnsindex) {
        // if Question index already exists
        if (values.questionIndex === activeQnIndex) {
          console.log(
            'Question already exists==>',
            values.questionIndex,
            savedAnsData[svdAnsindex],
          );
          //adding an answer key
          if (!savedAnsData[svdAnsindex].optionNo.includes(optionNoval)) {
            savedAnsData[svdAnsindex].optionNo.push(optionNoval);
          }
          // deleting the ans Key
          else {
            savedAnsData[svdAnsindex].optionNo.splice(
              savedAnsData[svdAnsindex].optionNo.indexOf(optionNoval),
              1,
            );
            //deleting the question is answers are empty
            if (savedAnsData[svdAnsindex].optionNo.length === 0) {
              savedAnsData.splice(savedAnsData.indexOf(svdAnsindex), 1);
            }
          }
          qstnIndxExists = true;
        }
      });
      //if the Question index is new
      if (qstnIndxExists === false) {
        console.log('New question ansrs added', activeQnIndex);
        savedAnsData.push(AnsweredQnData);
      }
    } else {
      savedAnsData.push(AnsweredQnData);
      console.log('Started adding andwers data');
    }

    //saving data everytime option is clicked
    this.setState({answerData: savedAnsData});

    console.log('Stored answers===>', this.state.answerData);

    //alert(optionNo)
    switch (optionNoval) {
      case 0:
        this.setState({option1: !this.state.option1});
        break;
      case 1:
        this.setState({option2: !this.state.option2});
        break;
      case 2:
        this.setState({option3: !this.state.option3});
        break;
      case 3:
        this.setState({option4: !this.state.option4});
        break;
      case 4:
        this.setState({option5: !this.state.option5});
        break;
    }
  };

  //Checking answer of each questions
  checkAnswer = () => {
    let currentqnId = this.state.activeQn - 1;
    Alert.alert(
      'Correct Answer: ',
      this.state.allQuestions.data.details[currentqnId].answerExplained,
    );
  };

  //Loading question and and on individual question button click
  ShowQuestion = qsnId => {
    let activeQuestion = qsnId;
    qsnId = qsnId - 1;
    console.log(
      'active Question data ==>',
      this.state.allQuestions.data.details[qsnId].answer,
    );
    console.log(
      'active Question data ==>',
      this.state.allQuestions.data.details[qsnId].correct_answer,
    );
    // console.log('showing quieation data===> ', {
    //   qid: qsnId,
    //   question: this.state.allQuestions.data.details[qsnId].question,
    //   allAnswers: this.state.allQuestions.data.details[qsnId].answer,
    //   ansOptionsCount: this.state.allQuestions.data.details[qsnId].answer
    //     .length,
    //   activeQn: activeQuestion,
    //   currentQnId: this.state.allQuestions.data.details[qsnId].quizId,
    //   option1: false,
    //   option2: false,
    //   option3: false,
    //   option4: false,
    // });
    let answersChecked = [];
    console.log('Saved question Index==>', activeQuestion);
    this.state.answerData.map(function(value) {
      if (value.questionIndex === activeQuestion) {
        answersChecked = value.optionNo;
      }
    });
    this.setState({
      question: this.state.allQuestions.data.details[qsnId].question,
      allAnswers: this.state.allQuestions.data.details[qsnId].answer,
      activeQnCorrectAns: null,
      activeQn: activeQuestion,
      currentQnId: this.state.allQuestions.data.details[qsnId].quizId,
      option1: answersChecked.includes(0) ? true : false,
      option2: answersChecked.includes(1) ? true : false,
      option3: answersChecked.includes(2) ? true : false,
      option4: answersChecked.includes(3) ? true : false,
    });
  };

  submittingAnswers = () => {
    // this.props.navigation.navigate('Selectquiz');
    // return;
    let navVar = this.props;
    this.setState({spinner: true});
    setTimeout(function() {
      //this.setState({modalVisible: !this.state.modalVisible});
      navVar.navigation.navigate('Selectquiz');
    }, 3000);
  };

  reportThisQuestionModal = currentQnIndex => {
    console.log('Current Question Index: ' + currentQnIndex);
    this.setState({reportQnModalVisible: true});
  };

  reportThisQuestion = async () => {
    let url = null;
    let questionid = this.state.currentQnId;
    let quizid = this.state.quizId;
    let reason = this.state.reportReason;

    if (reason === null || reason == '') {
      Alert.alert('Alert', 'Please add a reason to report this question');
      return;
    }
    this.setState({spinner: true, modalMessage: 'Submitting report...'});

    let userDetails = await AsyncStorage.getItem('userdetails');
    userDetails = JSON.parse(userDetails);
    let userid = userDetails.userId;

    url =
      baseUrl +
      '/report_a_question/?secret_key=r4zJswPsVA9KKlDMSbA8sPbXAdw7YMfQ' +
      '&&questionid=' +
      questionid +
      '&&quizid=' +
      quizid +
      '&&userid=' +
      userid +
      '&&reason=' +
      reason;

    console.log(url);

    fetch(url, {
      method: 'GET',
    })
      .then(response => response.json())
      .then(responseJson => {
        this.setState({spinner: false, modalMessage: 'Loading Quiz...'});
        if (responseJson.data === 'Question has been reported') {
          Alert.alert(
            'Success',
            'Report has been submitted',
            [
              {
                text: 'OK',
                onPress: () => this.setState({reportQnModalVisible: false}),
              },
            ],
            {cancelable: false},
          );
        }
        console.log(responseJson);
      })
      .catch(error => {
        console.error(error);
      });
  };

  render() {
    var qnsNumbers = [];
    for (let i = 1; i <= this.state.totalQuestions; i++) {
      //answerData
      let questionColor = styles.nonactiveQn;
      this.state.answerData.map(function(ansDataValue, index) {
        if (ansDataValue.questionIndex === i) {
          questionColor = styles.answeredQuestion;
        }
      });
      if (i === this.state.activeQn) {
        questionColor = styles.activeQn;
      }
      qnsNumbers.push(
        <TouchableOpacity
          onPress={() => this.ShowQuestion(i)}
          style={styles.pagination}>
          <Text style={questionColor}>{i}</Text>
        </TouchableOpacity>,
      );
    }
    var ansOptions = [];

    for (let i = 0; i < this.state.allAnswers.length; i++) {
      
      let optionName = '';
      switch (i) {
        case 0:
          optionName = this.state.option1;
          break;
        case 1:
          optionName = this.state.option2;
          break;
        case 2:
          optionName = this.state.option3;
          break;
        case 3:
          optionName = this.state.option4;
          break;
        case 4:
          optionName = this.state.option5;
          break;
      }
      
      
      let checkedColorbox = '#000';
      let checkboxColorbg = '#f9f9f9';
      let correctColor = '#35f400';
      let incorrectColor = '#e1403f';
      let checkedColorForWrong = '#000';
      let questionColor = styles.nonactiveQn;
      let currentqnId = this.state.activeQn - 1;

      let a = this.state.allQuestions.data.details[currentqnId].correct_answer;
      console.log("answedata===>", a);
      checkboxColorbg =
        i === this.state.activeQnCorrectAns ? correctColor : checkboxColorbg;
      checkboxColorbg =
        optionName === true && i !== this.state.activeQnCorrectAns
          ? incorrectColor
          : checkboxColorbg;
      checkedColorbox =
        optionName === true && i !== this.state.activeQnCorrectAns
          ? checkedColorForWrong
          : checkedColorbox;

        checkboxColorbg =
          optionName === true && i === this.state.allQuestions.data.details[currentqnId].correct_answer
           ? correctColor
           : checkboxColorbg;
      ansOptions.push(
        <CheckBox
          center
          checkedColor={checkedColorbox}
          containerStyle={{backgroundColor: checkboxColorbg}}
          title={this.state.allAnswers[i]}
          checked={optionName}
          onPress={() => this.updatingOptions(i)}
        />,
      );
    }
      
      
  
    return (
      <View style={styles.container}>
        <Modal
          animationType="slide"
          transparent={false}
          visible={this.state.modalVisible}
          onRequestClose={() => {
            Alert.alert('Modal has been closed.');
          }}>
          <View
            style={{
              flex: 1,
              felxDirection: 'column',
              marginTop: 22,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <View>
              <Text style={{fontSize: 20}}>{this.state.modalMessage}</Text>
            </View>
          </View>
        </Modal>

        <Modal
          animationType="fade"
          transparent={false}
          visible={this.state.reportQnModalVisible}
          onRequestClose={() => {
            () => this.setState({reportQnModalVisible: false});
          }}>
          <View
            style={{
              flex: 1,
              felxDirection: 'column',
              marginTop: 22,
              justifyContent: 'center',
              alignItems: 'center',
              alignContent: 'space-between',
              paddingHorizontal: 20,
            }}>
            <Text style={{fontSize: 20, marginVertical: 40}}>
              Add a reason to report this question
            </Text>
            <View style={{width: '100%'}}>
              <TextInput
                multiline={true}
                numberOfLines={8}
                placeholder={'Write the issue here'}
                placeholderTextColor={'#ccc'}
                style={styles.formInputs}
                onChangeText={reportReason => this.setState({reportReason})}
              />
            </View>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                width: '100%',
              }}>
              <TouchableOpacity
                onPress={() => this.reportThisQuestion()}
                style={{alignItems: 'center', justifyContent: 'center'}}>
                <Text style={[styles.buttons]}>Submit</Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={{alignItems: 'center', justifyContent: 'center'}}
                onPress={() => this.setState({reportQnModalVisible: false})}>
                <Text style={[styles.buttons]}>Close</Text>
              </TouchableOpacity>
            </View>
          </View>
        </Modal>
        <Spinner
          visible={this.state.spinner}
          textContent={this.state.modalMessage}
          textStyle={styles.spinnerTextStyle}
        />
        <View style={styles.quizTitle}>
          <Text>
            <Text>{this.state.quizName}</Text>
            <Text>
              {'   '}( {this.state.activeQn}/{this.state.totalQuestions} )
            </Text>
          </Text>
        </View>
        <ScrollView style={{width: '100%', marginBottom: 50}}>
          <View style={styles.contentBox}>
            <Text style={styles.titleCont}>{this.state.QuizName}</Text>
          </View>
          <View style={styles.contentBox}>
            <Text style={styles.bluebar}>.</Text>
            <Text style={styles.titleCont}>Question</Text>
            <Text style={styles.questionCont}>{this.state.question}</Text>
          </View>
          <View style={[styles.contentBox, styles.answerBox]}>
            <Text style={styles.bluebar}>.</Text>
            <Text style={styles.titleCont}> Answers </Text>
            <View style={{alignItems: 'flex-start', width: '100%'}}>
              {ansOptions}
            </View>
          </View>
          <View
            style={{
              flex: 2,
              flexDirection: 'row',
              justifyContent: 'space-between',
              paddingHorizontal: 40,
              paddingBottom: 50,
            }}>
            <TouchableOpacity
              onPress={() => this.checkAnswer()}
              style={styles.buttons}>
              <Text style={styles.buttonsText}>Rational</Text>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => this.loadNextQuestion(+this.state.activeQn + 1)}
              style={styles.buttons}>
              {this.state.activeQn === this.state.totalQuestions ? (
                <Text style={styles.buttonsText}>Finish Quiz</Text>
              ) : (
                <Text style={styles.buttonsText}>Next</Text>
              )}
            </TouchableOpacity>
          </View>
          <View>
            <TouchableOpacity
              onPress={() => this.reportThisQuestionModal(this.state.activeQn)}
              style={styles.ReportButtons}>
              <Text style={{color: '#fff'}}>Report this question</Text>
            </TouchableOpacity>
          </View>
        </ScrollView>
        <ScrollView
          horizontal={true}
          style={{
            position: 'absolute',
            backgroundColor: '#ccc',
            bottom: 0,
            width: '100%',
            left: 0,
          }}>
          <View
            style={{flex: 1, flexDirection: 'row', width: '100%', height: 45}}>
            {qnsNumbers}
          </View>
        </ScrollView>
      </View>
    );
  }
}


const styles = StyleSheet.create({
  container: {
    paddingTop: 0,
    flex: 2,
    flexDirection: 'column',
    alignItems: 'center',
    width: '100%',
  },
  formInputs: {
    width: '100%',
    borderWidth: 1,
    borderColor: '#ccc',
    marginBottom: 20,
    color: '#000',
    fontSize: 15,
    borderRadius: 20,
  },
  quizTitle: {
    paddingHorizontal: 20,
    paddingVertical: 5,
    backgroundColor: '#f9f9f9',
    width: '100%',
  },
  answerBox: {
    marginTop: 100,
    marginBottom: 50,
  },
  pagination: {
    margin: 5,
    backgroundColor: '#f9f9f9',
    height: 29,
  },
  nonactiveQn: {
    paddingHorizontal: 8,
    paddingVertical: 5,
  },
  activeQn: {
    paddingHorizontal: 8,
    paddingVertical: 6,
    backgroundColor: '#49d04a',
    color: '#fff',
  },
  answeredQuestion: {
    paddingHorizontal: 8,
    paddingVertical: 6,
    backgroundColor: '#ff9900',
    color: '#fff',
  },
  buttons: {
    backgroundColor: '#49d04a',
    paddingHorizontal: 40,
    paddingVertical: 8,
    borderRadius: 50,
  },
  ReportButtons: {
    backgroundColor: '#1a2c71',
    paddingHorizontal: 40,
    paddingVertical: 8,
    borderRadius: 50,
    width: 'auto',
    marginHorizontal: 40,
    alignItems: 'center',
  },
  buttonsText: {
    color: '#ffffff',
    fontSize: 20,
  },
  titleCont: {
    fontSize: 24,
    marginBottom: 10,
  },
  contentBox: {
    paddingHorizontal: 20,
    width: '100%',
  },
  bluebar: {
    height: 100,
    backgroundColor: 'blue',
    width: 5,
    position: 'absolute',
  },
  questionCont: {
    fontSize: 18,
  },
  spinnerTextStyle: {
    color: '#FFF',
  },
});
