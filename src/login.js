/* eslint-disable react-native/no-inline-styles */
/* eslint-disable prettier/prettier */
import React, { Component } from 'react';
import { View, Text, TouchableOpacity, StyleSheet, TextInput, Image, Alert } from 'react-native';
import Spinner from 'react-native-loading-spinner-overlay';
import Icon from 'react-native-vector-icons/Ionicons';
import {baseUrl} from './constants/HttpConsts';
import AsyncStorage from '@react-native-community/async-storage';


export default class Login extends Component {
  static navigationOptions = {
    title: null,
    header: null,
  };
  constructor(props) {
    super(props);
    this.state = {
      UserEmail: '',
      UserPassword: '',
      spinner: false,
      showPassword: false,
    };
  }

  showPassword = () => {
    this.setState({ showPassword: !this.state.showPassword });
  }

  loginCheck = () => {
    this.setState({ spinner: true });
    const apiLogin = baseUrl + '/wp_generate_auth_cookie/?secret_key=r4zJswPsVA9KKlDMSbA8sPbXAdw7YMfQ';

    let userContent = JSON.stringify({
      username: this.state.UserEmail,
      password: this.state.UserPassword,
    });

    fetch(apiLogin, {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
      body: userContent,

    }).then((response) => response.json())
      .then((responseJson) => {
        if (responseJson.data.message === 'successfully logged in') {
          console.log('Success login resonse ===>', responseJson);
          console.log('login page saved data ===>', JSON.stringify({
            username: responseJson.data.username,
            userId: responseJson.data.id,
            name: responseJson.data.displayname,
            email: responseJson.data.email,
            roleId: null,
          }));

          AsyncStorage.setItem(
            'userdetails',
            JSON.stringify({
              username: responseJson.data.username,
              userId: responseJson.data.id,
              name: responseJson.data.displayname,
              email: responseJson.data.email,
              roleId: null,
            }),
          );

          this.props.navigation.navigate('Selectquiz');
        } else {
          console.log(responseJson);
          Alert.alert('Message', 'Username or Password is incorrect');
        }
        this.setState({ spinner: false });
      }).catch((error) => {
        if (this.state.UserEmail === '' || this.state.UserPassword === '') {
          Alert.alert('Message', 'An error has occured, please contact admin');
        }
        console.error(error);
        this.setState({ spinner: false });
      });
  }

  render() {
    const { navigate } = this.props.navigation;
    return (
      <View style={styles.container}>
        <Spinner
          visible={this.state.spinner}
          textContent={'Loading...'}
          textStyle={styles.spinnerTextStyle}
        />
        <Image
          style={styles.logoImage}
          source={require('./images/passtheot-logo.png')}
        />
        <Text style={styles.titleCont}>LOGIN</Text>
        <View style={styles.inputWrap}>
          <TextInput
            placeholder={'UserName'}
            placeholderTextColor={'#fff'}
            style={styles.formInputs}
            onChangeText={(UserEmail) => this.setState({ UserEmail })}
          />
          <Icon name="ios-person" style={styles.eyeIcon} size={30} color="#fff" />
        </View>
        <View style={styles.inputWrap}>
          <TextInput
            secureTextEntry={!this.state.showPassword}
            placeholder={'Password'}
            placeholderTextColor={'#fff'}
            style={styles.formInputs}
            onChangeText={(UserPassword) => this.setState({ UserPassword })}
          />
          { this.state.showPassword && <Icon name="ios-eye" style={styles.eyeIcon} size={30} color="#fff" onPress={ ()=> this.showPassword() } />}
          { !this.state.showPassword && <Icon name="ios-eye-off" style={styles.eyeIcon} size={30} color="#fff" onPress={ ()=> this.showPassword() } />}
        </View>
        <TouchableOpacity
          style={styles.loginButton}
          onPress={() => this.loginCheck()}>
          <Text style={styles.buttontext}> Log in </Text>
        </TouchableOpacity>
        <View style={ styles.registerButton }>
          <TouchableOpacity
            onPress={() => navigate('Register')}
            style={{ justifyContent: 'flex-start' }}>
            <Text style={{ color: '#fff' }}>
              {/* Register */}
            </Text>
          </TouchableOpacity>
        </View>
        <View style={ styles.registerInfoButton }>
          <TouchableOpacity
            onPress={() => navigate('Privacy')}>
            <Text style={{ color: '#fff' }}>
            Privacy Policy
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => navigate('Terms')}>
            <Text style={{ color: '#fff' }}>
              Terms and Conditions
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    flexDirection: 'column',
    alignItems: 'center',
    backgroundColor: '#213466',
  },
  registerButton:{
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    paddingTop: 40,
    width: '100%',
    paddingLeft: '10%',
  },
  registerInfoButton:{
    flex: 2,
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingTop: 40,
    width: '100%',
    paddingHorizontal: '10%',
  },
  logoImage: {
    width: '40%',
    height: 30,
    marginBottom: 40,
    marginTop: '30%',
  },
  loginButton: {
    backgroundColor: '#fff',
    paddingHorizontal: 40,
    paddingVertical: 8,
    borderRadius: 50,
    marginTop: 20,
  },
  buttontext: {
    fontSize: 20,
  },
  inputWrap: {
    width: '80%',
    position: 'relative',
  },
  eyeIcon: {
    position: 'absolute',
    right: 5,
    top: 12,
  },
  formInputs: {
    width: '100%',
    borderBottomWidth: 1,
    borderColor: '#ccc',
    marginBottom: 20,
    color: '#fff',
  },
  titleCont: {
    color: '#fff',
    fontSize: 24,
    marginBottom: 40,
  },
  spinnerTextStyle: {
    color: '#FFF',
  },
});
