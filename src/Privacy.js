import React, {Component} from 'react';
import {ScrollView, Text} from 'react-native';

export default class Privacy extends Component {
  static navigationOptions = {
    title: 'Login',
  };
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      // eslint-disable-next-line react-native/no-inline-styles
      <ScrollView style={{flex: 1, padding: 20}}>
        <Text style={{fontSize: 30, paddingBottom: 30, textAlign: 'center'}}>Privacy Policy</Text>
        <Text>Updated May 2018</Text>
        <Text />
        <Text>Pass the OT LLC Website Global Privacy Policy </Text>
        <Text />
        <Text>
          Thank you for choosing to be part of our community at Pass the OT LLC
          (“passtheot.com”). We are committed to protecting your personal
          information and your right to privacy. If you have any questions or
          concerns about our policy, or our practices with regards to your
          personal information, please contact us at privacy@passtheot.com
        </Text>
        <Text />
        <Text>
          In this privacy notice, we describe our privacy policy. We seek to
          explain to you in the clearest way possible what information we
          collect, how we use it and what rights you have in relation to it. If
          there are any terms in this privacy policy that you do not agree with,
          please discontinue use of our Sites and our services. It is Pass the
          OT’s policy to comply with all applicable privacy and data protection
          laws. This commitment reflects the value we place on earning and
          keeping the trust of our employees, customers, business partners and
          others who share their personal information with us.
        </Text>
        <Text />
        <Text>
          This Global Privacy Policy (this “Policy”) describes how Pass the OT
          LLC protects your privacy when we collect personal information on Pass
          the OT Internet sites (such as passtheot.com). As used in this Policy,
          the term “personal information” means information that identifies you
          personally, alone or in combination with other information available
          to us.
        </Text>
        <Text />
        <Text>Your Consent</Text>
        <Text />
        <Text>
          By visiting a Pass the OT website or providing personal information to
          us, you are consenting to the collection, use and disclosure of your
          personal information as described in this Policy. If you do not
          consent to the collection, use and disclosure of your personal
          information as described in this Policy (and any applicable Country or
          Web Site Privacy Statements), do not use Pass the OT Internet Sites,
          contact Pass the OT, or Submit Data to jshane@passtheot.com via
          Internet form.
        </Text>
        <Text />
        <Text>
          Please read this privacy policy carefully as it will help you make
          informed decisions about sharing your personal information with us.
        </Text>
        <Text />
        <Text>TABLE OF CONTENTS</Text>
        <Text />
        <Text>WHAT INFORMATION DO WE COLLECT?</Text>
        <Text>HOW DO WE USE YOUR INFORMATION?</Text>
        <Text>WILL YOUR INFORMATION BE SHARED WITH ANYONE?</Text>
        <Text>DO WE USE COOKIES AND OTHER TRACKING TECHNOLOGIES?</Text>
        <Text>HOW LONG DO WE KEEP YOUR INFORMATION?</Text>
        <Text>HOW DO WE KEEP YOUR INFORMATION SAFE?</Text>
        <Text>WHAT ARE YOUR PRIVACY RIGHTS?</Text>
        <Text>DO WE MAKE UPDATES TO THIS POLICY?</Text>
        <Text>HOW CAN YOU CONTACT US ABOUT THIS POLICY?</Text>
        <Text>
          Limitations on the Collection, Use, and Disclosure of Personal
          Information
        </Text>
        <Text />
        <Text>
          To the extent required by applicable law, whenever Pass the OT
          collects personal information on an Pass the OT Internet Site, Pass
          the OT will:
        </Text>
        <Text />
        <Text>
          provide timely and appropriate notice to you about its data practices;
        </Text>
        <Text>
          collect, use, disclose and transfer your personal information only
          with your consent, which may be express or implied, depending on the
          sensitivity of the personal information, legal requirements, and other
          factors;
        </Text>
        <Text>
          collect your personal information only for specific, limited purposes.
          The information we collect will be relevant, adequate and not
          excessive for the purposes for which it is collected;
        </Text>
        <Text>
          process your personal information in a manner consistent with the
          purposes for which it was originally collected or to which you have
          subsequently consented;
        </Text>
        <Text>
          take commercially reasonable steps to ensure that your personal
          information is reliable for its intended use, accurate, complete, and,
          where necessary, kept up-to-date;
        </Text>
        <Text>WHAT INFORMATION DO WE COLLECT?</Text>
        <Text>Personal Information you disclose to us</Text>
        <Text />
        <Text>
          In short: We collect personal information that you provide to us such
          as name, address, contact information, school attended, study preps
          used, previous NBCOT® exam test scores, passing NBCOT® test scores,
          NBCOT® exam history, your program director contact information,
          personal details, company, and payment information.
        </Text>
        <Text />
        <Text>
          We collect personal information that you voluntarily provide to us
          when expressing an interest in obtaining information about us or our
          products and services, when participating in activities on the Sites
          or otherwise contacting us.
        </Text>
        <Text />
        <Text>
          The personal information that we collect depends on the context of
          your interactions with us and the Sites, the choices you make and the
          products and features you use. The personal information we collect can
          include the following:
        </Text>
        <Text />
        <Text>
          Name and Contact Data. We collect your first and last name, email
          address, phone number, and other similar contact data.
        </Text>
        <Text />
        <Text>
          Payment Data. We collect data necessary to process your payment if you
          make purchases, such as a credit card number, and the security code
          associated with your payment instrument. All payment data is stored by
          our payment processor and you should review its privacy policies and
          contact the payment processor directly to respond to your questions.
        </Text>
        <Text />
        <Text>
          All personal information that you provide to us must be true, complete
          and accurate, and you must notify us of any changes to such personal
          information.
        </Text>
        <Text />
        <Text>Information automatically collected</Text>
        <Text />
        <Text>
          In Short: Some information – such as IP address and/or browser and
          device characteristics is automatically collected when you visit our
          Sites.
        </Text>
        <Text />
        <Text>
          Like many businesses, when you access and use an Pass the OT Sites,
          there are 3 ways we may collect information about you:
        </Text>
        <Text />
        <Text>Information Sent to Us by Your Web Browser</Text>
        <Text>
          Pass the OT collects information that is sent to us automatically by
          your web browser. This information typically includes the IP address
          of your Internet service provider, the name of your operating system,
          browser and device characteristics, referring URLs, device name,
          country, location, information about how and when you use our Sites
          and other technical information. The information we receive depends on
          the settings on your web browser. Please check your browser if you
          want to learn what information your browser sends or how to change
          your settings. The information provided by your browser does not
          identify you personally. We use this information to create statistics
          that help us improve our sites and make them more compatible with the
          technology used by our Internet visitors
        </Text>
        <Text />
        <Text>Information You Knowingly and Voluntarily Provide</Text>
        <Text>
          Pass the OT collects the information you knowingly and voluntarily
          provide when you use a Pass the OT Internet Site, for example, the
          information you provide when you sign up to receive email alerts, when
          you complete a survey, or when you ask us a question or email us with
          feedback. In many cases, this information will be personal
          information. Pass the OT uses this information for the purposes for
          which you provide it. We may also use the information we collect on
          Pass the OT Internet Sites for various business purposes such as
          customer service, fraud prevention, market research, improving our
          products and services, and providing you and your company with
          information and offers we believe may be of interest to you. We may
          also remove all the personally identifiable information and use the
          rest for historical, statistical or scientific purposes.
        </Text>
        <Text />
        <Text>Information collected from other sources</Text>
        <Text />
        <Text>
          In Short: We may collect limited data from public databases, marketing
          partners, and other outside sources.
        </Text>
        <Text />
        <Text>
          We may obtain information about you from other sources, such as public
          databases, joint marketing partners, as well as other third parties.
          Examples of the information we receive from other sources include:
          social media profile information, marketing leads and search results,
          and links, including paid listings (such as sponsored links).
        </Text>
        <Text />
        <Text>Links to Third Party Internet Sites</Text>
        <Text />
        <Text>
          Pass the OT Internet Sites may contain links to Internet sites that
          are not operated by Pass the OT. These links are provided as a service
          and do not imply any endorsement of the activities or content of these
          sites, nor any association with their operators. Pass the OT does not
          control these Internet sites and is not responsible for their content,
          security, or privacy practices. We urge you to review the privacy
          policy posted on websites you visit before using the site or providing
          personal information.
        </Text>
        <Text />
        <Text>HOW DO WE USE YOUR INFORMATION?</Text>
        <Text>
          In Short: We process your information for purposes based on legitimate
          business interests, the fulfillment of our contract with you,
          compliance with our legal obligations, and/or your consent.
        </Text>
        <Text />
        <Text>
          We use personal information collected via our Sites for a variety of
          business purposes described below. We process your personal
          information for these purposes in reliance on our legitimate business
          interests (“Business Purposes”), in order to enter into or perform a
          contract with you (“Contractual”), with your consent (“Consent”),
          and/or for compliance with our legal obligations (“Legal Reasons”). We
          indicate the specific processing grounds we rely on next to each
          purpose listed below.
        </Text>
        <Text />
        <Text>We use the information we collect or receive:</Text>
        <Text />
        <Text>
          To send you marketing and promotional communications for Business
          Purposes. We and/or our third-party marketing partners may use the
          personal information you send to us for our marketing purposes, if
          this is in accordance with your marketing preferences. You can opt-out
          of our marketing emails at any time.{' '}
        </Text>
        <Text>
          To send administrative information to you for Business Purposes and/or
          Legal Reasons. We may use your personal information to send you
          product, service, and new feature information and/or information about
          changes to our terms, conditions, and policies.
        </Text>
        <Text>
          Deliver targeted advertising to you for our Business Purposes. We may
          use your information to develop and display content and advertising
          (and work with third parties who do so) tailored to your interests
          and/or location and to measure its effectiveness.
        </Text>
        <Text>
          Request feedback for our Business Purposes. We may use your
          information to request feedback and to contact you about your use of
          our Sites.
        </Text>
        <Text>
          To protect our Sites for our Business Purposes. We may use your
          information as part of our efforts to keep our Sites safe and secure.
        </Text>
        <Text>
          To enforce our terms, conditions, and policies for Business Purposes
          and/or for Legal Reasons.
        </Text>
        <Text>
          To respond to legal requests and prevent harm for Legal Reasons. If we
          receive a subpoena or other legal request, we may need to inspect the
          data we hold to determine how to respond.
        </Text>
        <Text>
          For other Business Purposes. We may use your information for other
          Business Purposes, such as data analysis, identifying usage trends,
          determining the effectiveness of our promotional campaigns and to
          evaluate and improve our Sites, products, services, marketing, and
          your experience.
        </Text>
        <Text>WILL YOUR INFORMATION BE SHARED WITH ANYONE?</Text>
        <Text>
          In Short: We only share information with your consent, to comply with
          laws, to protect your rights, or to fulfill business obligations.
        </Text>
        <Text />
        <Text>
          Pass the OT will never sell personal information collected from its
          Internet sites to mailing list brokers or other third parties. Pass
          the OT may share your personal information with third parties we hire
          to perform services for us. These third parties are required to use
          the personal information we share with them only to perform services
          on our behalf and to treat your personal information as strictly
          confidential. In some cases, Pass the OT may share your personal
          information with third parties who partner with us to provide products
          and services to our customers. When we do so, we will require our
          business partners to use the personal information we share with them
          in a manner consistent with the purposes for which it was originally
          collected (or to which you subsequently consented) and only as
          permitted under this Policy, any applicable Country or Web Site
          Privacy Statements, and all applicable privacy and data protection
          laws. In certain, limited circumstances we may share or transfer
          personal information to unrelated third parties.
        </Text>
        <Text />
        <Text>
          For example, we may provide personal information to a third party for
          the following reasons:
        </Text>
        <Text />
        <Text>at your request.</Text>
        <Text>to comply with a legal requirement or court order.</Text>
        <Text>to investigate a possible crime, such as identity theft.</Text>
        <Text>
          in connection with the sale, purchase, merger, reorganization,
          liquidation or dissolution of Pass the OT or a Pass the OT business
          unit.
        </Text>
        <Text>
          under similar circumstances. If such an event occurs, we will take
          appropriate steps to protect your personal information.
        </Text>
        <Text>DO WE USE COOKIES AND OTHER TRACKING TECHNOLOGIES?</Text>
        <Text>
          In Short: Yes, we use cookies and other tracking technologies to
          collect and store your information.
        </Text>
        <Text />
        <Text>
          Information Collected by Placing a “Cookie” On Your Computer
        </Text>
        <Text />
        <Text>
          Pass the OT may obtain information about you by installing a “tag” on
          your computer’s hard drive. This tag is known as a “cookie.” All Pass
          the OT Internet Sites use “session cookies.” A session cookie is used
          to tag your computer with a computer-generated, unique identifier when
          you access our site. A session cookie does not identify you personally
          and expires after you close your browser. We use session cookies to
          collect statistical information about the ways visitors use our
          sites’, which pages they visit, which links they use, and how long
          they stay on each page. We analyze this information in statistical
          form to better understand our visitor’s interests and needs to improve
          the content and functionality of our sites. Some Pass the OT Internet
          sites also use “persistent cookies.” These cookies do not expire when
          you close your browser; they stay on your computer until you delete
          them. By assigning your computer a unique identifier, we are able to
          create a database of your previous choices and preferences, and in
          situations where these choices or preferences need to be collected
          again, they can be provided by us automatically, saving you time and
          effort. If you do not wish to receive cookies, you may set your
          browser to reject cookies or to alert you when a cookie is placed on
          your computer. Although you are not required to accept cookies when
          you visit a Pass the OT Internet Site, you may be unable to use all of
          the functionality of the site if your browser rejects our cookies.
        </Text>
        <Text />
        <Text>HOW LONG DO WE KEEP YOUR INFORMATION?</Text>
        <Text>
          In Short: We keep your information for as long as necessary to fulfill
          the purposes outlined in this privacy policy unless otherwise required
          by law.
        </Text>
        <Text />
        <Text>
          Pass the OT retains all personal information collected on Pass the OT
          Internet Sites and web forms as long as necessary to provide the
          services, products, and information you request or as permitted by
          applicable law.
        </Text>
        <Text />
        <Text>
          We respond to all requests we receive from individuals wishing to
          exercise their data protection rights in accordance with applicable
          data protection laws. Notwithstanding the foregoing, we reserve the
          right to keep any information in our archives that we deem necessary
          to comply with our legal obligations, resolve disputes and enforce our
          agreements.
        </Text>
        <Text />
        <Text>Access to Personal Information</Text>
        <Text />
        <Text>
          You may review, correct and update the personal information that you
          provide to us by writing to us at Pass the OT LLC, Global Privacy
          Policy,2711 N Sepulveda Blvd # 431, Manhattan Beach, California,
          90232, USA or emailing us at privacy@passtheot.com
        </Text>
        <Text />
        <Text>HOW DO WE KEEP YOUR INFORMATION SAFE?</Text>
        <Text>
          In Short: We aim to protect your personal information through a system
          of organizational and technical security measures.
        </Text>
        <Text />
        <Text>
          Your personal information will generally be stored in Pass the OT
          databases or databases maintained by our service providers. Most of
          these databases are stored on servers located in the United States. To
          the extent required by law, if your personal information will be
          transferred outside your country an appropriate notice will be
          provided. Pass the OT maintains reasonable safeguards to protect the
          confidentiality, security, and integrity of your personal information.
          For example, we use secure socket layer (SSL) technology to transfer
          personal information over the Internet. Although we use security
          measures to help protect your personal information against
          unauthorized disclosure, misuse or alteration, as is the case with all
          computer networks linked to the Internet, we cannot guarantee the
          security of information provided over the Internet and will not be
          responsible for breaches of security beyond our reasonable control.
        </Text>
        <Text />
        <Text>WHAT ARE YOUR PRIVACY RIGHTS?</Text>
        <Text>
          In Short: In some regions, such as the European Economic Area, you
          have rights that allow you greater access to and control you’re your
          personal information. You may review, change, or terminate your
          account at any time.
        </Text>
        <Text />
        <Text>
          If you are a resident of the European Economic Area, you have the
          following data protection rights:
        </Text>
        <Text />
        <Text>
          If you wish to access, correct, update or request deletion of your
          personal information, you can do so at any time by contacting us using
          the contact details provided under the Contact Pass the OT below.
        </Text>
        <Text>
          In addition, you can object to the processing of your personal
          information, ask us to restrict processing of your personal
          information or request portability of your personal information.
          Again, you can exercise these rights by contacting us using the
          contact details provided under Contact Pass the OT below.
        </Text>
        <Text>
          You have the right to opt-out of marketing communications we send you
          at any time. You can exercise this right by clicking on the
          “unsubscribe” or “opt-out” link in the marketing emails we send you.
          Registered users can manage their account settings and email marketing
          preferences as described in the “Choices Regarding Your Personal
          Information” section below. If you are an unregistered user, or to
          opt-out of other forms of marketing (such as postal marketing or
          telemarketing), you may contact us using the contact details provided
          under Contact Pass the OT below.
        </Text>
        <Text>
          Similarly, if we have collected and processed your personal
          information with your consent, then you can withdraw your consent at
          any time. Withdrawing your consent will not affect the lawfulness of
          any processing we conducted prior to your withdrawal, nor will it
          affect processing of your personal information conducted in reliance
          on lawful processing grounds other than consent.
        </Text>
        <Text>
          You have the right to complain to a data protection authority about
          our collection and use of your personal information. For more
          information, please contact your local data protection authority.
        </Text>
        <Text>
          Cookies and similar technologies: Most Web browsers are set to accept
          cookies by default. If you prefer, you can usually choose to set your
          browser to remove cookies and to reject cookies.
        </Text>
        <Text />
        <Text>DO WE MAKE UPDATES TO THIS POLICY?</Text>
        <Text>
          In Short: Yes, we will make updates to this policy as necessary to
          stay compliant with relevant laws.
        </Text>
        <Text />
        <Text>
          Pass the OT reserves the right, at its sole discretion, to change,
          modify, add or remove any portion of this policy, in whole or in part,
          at any time. Changes in Policy will be effective when posted. You
          agree to review this Policy periodically to be aware of any changes.
          Your continued use of the Site after any changes to this Policy will
          be considered acceptance of those changes.
        </Text>
        <Text />
        <Text>HOW CAN YOU CONTACT US ABOUT THIS POLICY?</Text>
        <Text>
          If you have any questions about this Policy or our use of your
          personal information, you may email us at privacy@passtheot.com or by
          post to:
        </Text>
        <Text />
        <Text>Pass the OT LLC.,</Text>
        <Text />
        <Text>Global Privacy Policy</Text>
        <Text />
        <Text>2711 N Sepulveda Blvd. #431</Text>
        <Text />
        <Text>Manhattan Beach, CA 90266</Text>
        <Text />
        <Text>USA </Text>
      </ScrollView>
    );
  }
}
