import {createStackNavigator} from 'react-navigation-stack';
import Questionaries from './src/Questionaries';
import Login from './src/login';
import Register from './src/Register';
import Selectquiz from './src/Selectquiz';
//import HTML from 'react-native-render-html';
import Privacy from './src/Privacy';
import Terms from './src/Terms';
import {createAppContainer} from 'react-navigation';

const MainNavigator = createStackNavigator({
  Login: {screen: Login},
  Register: {screen: Register},
  Selectquiz: {screen: Selectquiz},
  Questionaries: {screen: Questionaries},
  Privacy: {screen: Privacy},
  Terms: {screen: Terms},
});

const App = createAppContainer(MainNavigator);

export default App;
